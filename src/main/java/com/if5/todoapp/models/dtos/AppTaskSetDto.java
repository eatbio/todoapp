package com.if5.todoapp.models.dtos;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.if5.todoapp.models.entities.AppTask;
import com.if5.todoapp.models.entities.AppUser;
import com.if5.todoapp.models.enumeration.TaskStatus;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AppTaskSetDto {

	private Long id;
	@NotNull(message = "le lebelle d'une tache ne peut etre null")
	private String taskTitle;
	@NotNull(message = "la description d'une tache ne peut etre null")
	private String descriptionTask;
	private Date startDate;
	private Date endDate;
	private String reminderPlace;
	private Double latitude;
	private Double longitude;
	private TaskStatus taskStatus;
	private Long user;
	private Long parentTask;
	
	
  
	public static AppTask buildTaskFromDto(AppTaskSetDto dto, AppTask task, AppUser user) {
		
		return AppTask.builder()
				.id(dto.getId() == 0 ? null : dto.getId())
				.taskTitle(dto.getTaskTitle())
				.descriptionTask(dto.getDescriptionTask())
				.startDate(dto.getStartDate())
				.endDate(dto.getEndDate())
				.reminderPlace(dto.getReminderPlace())
				.latitude(dto.getLatitude())
				.longitude(dto.getLongitude())
				.taskStatus(dto.getTaskStatus())
				.user(user)
				.parentTask(task)
				.build();
	}
	
}
