package com.if5.todoapp.models.dtos;


import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.if5.todoapp.models.entities.AppUser;
import com.if5.todoapp.models.entities.Role;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AppUserSetDto {
	
		private Long id;
		@NotNull(message = "le nom d'utilisateur est obligatoire")
		private String username;
		@NotNull(message = "le nom d'utilisateur est obligatoire")
		private String lastName;
		private String firstName;
		@NotNull(message = "le l'emeil de l'utilisateur est obligatoire")
		private String email;
		@NotNull(message = "le mot de passe est obligatoire")
		private String password;
		@NotNull(message = "vous devez entrer la confirmation du mot de passe")
		private String passwordConfirmation;
		private Integer phoneNumber;
		private String birthplace; 
		private Date birthDay;
		private String town;
		private String country;
		private Set<Long> roles;

		
		
	public static AppUser buildUserFromDto(AppUserSetDto dto , List<Role> roles) {
			
			return AppUser.builder()
					.username(dto.getUsername())
					.lastName(dto.getLastName())
					.firstName(dto.getFirstName())
					.email(dto.getEmail())
					.password(dto.getPassword())
					.passwordConfirmation(dto.getPasswordConfirmation())
					.phoneNumber(dto.getPhoneNumber())
					.birthplace(dto.getBirthplace())
					.birthDay(dto.getBirthDay())
					.town(dto.getTown())
					.country(dto.getCountry())
					.roles(roles)
					.build();
		}
		

}
