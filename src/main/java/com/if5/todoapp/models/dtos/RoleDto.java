package com.if5.todoapp.models.dtos;

import com.if5.todoapp.models.entities.Role;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleDto {
	private Long id;
	private String roleName;
	
	
	
	
	public static Role buildRoleFormDto(RoleDto dto) {
		
		return Role.builder()
				.id(dto.getId())
				.roleName(dto.getRoleName())
				.build();
		
	}
}
