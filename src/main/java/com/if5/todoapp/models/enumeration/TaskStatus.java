   package com.if5.todoapp.models.enumeration;

public enum TaskStatus {

	EN_COURS,
	REALISEE,
	EN_RETARD,
	ANNULE,
	EN_ATTENTE
}
