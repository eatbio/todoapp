package com.if5.todoapp.controllers.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.if5.todoapp.exceptions.InvalidEntityException;
import com.if5.todoapp.models.dtos.AppUserDto;
import com.if5.todoapp.models.dtos.AppUserSetDto;
import com.if5.todoapp.models.dtos.RoleDto;
import com.if5.todoapp.models.entities.AppUser;
import com.if5.todoapp.models.entities.Role;
import com.if5.todoapp.services.implementations.RoleServiceImpl;
import com.if5.todoapp.services.interfaces.AppUserServiceInterface;
import com.if5.todoapp.services.interfaces.RoleServiceInterface;

@RestController
@RequestMapping("toDoApp/V1")
public class UserController {

	@Autowired private AppUserServiceInterface appUserServiceInterface;
	@Autowired private RoleServiceInterface roleServiceInterface;
	
	@PostMapping("/userRegister")
	public ResponseEntity<AppUser> saveUser(@RequestBody @Valid AppUserSetDto appUserSetDto){
		System.out.println("Bonjour le monde");
		System.out.println(appUserSetDto.toString());
		if(!appUserSetDto.getPassword().equals(appUserSetDto.getPasswordConfirmation()) || appUserSetDto.getUsername() == null)
				                                         throw new RuntimeException("mot de passe incorrect");
		AppUser user = appUserServiceInterface.findByUsername(appUserSetDto.getUsername());
		if(user!=null)throw new RuntimeException("nom d'utilisateur deja present en base de donnée");
		appUserServiceInterface.saveUser(appUserSetDto);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/users")
	public ResponseEntity<List<AppUser>> getAllUsers (){
		return ResponseEntity.ok( appUserServiceInterface.getAllUsers());
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<AppUser> getUser(@PathVariable long id) throws InvalidEntityException{
		return ResponseEntity.ok(appUserServiceInterface.getUser(id));
	}
	
	@PostMapping("/save-role")
	public ResponseEntity<Role> saveRole(@RequestBody RoleDto roleDto){
		return ResponseEntity.ok(roleServiceInterface.sauveRole(roleDto));
	}
	
	@GetMapping("/roles")
	public ResponseEntity<List<Role>> getAllRoles(){
		
		return ResponseEntity.ok(roleServiceInterface.getAllRole());
	}
	
}
