package com.if5.todoapp.controllers.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.if5.todoapp.exceptions.EntityNotFoundException;
import com.if5.todoapp.exceptions.InvalidEntityException;
import com.if5.todoapp.models.dtos.AppTaskDto;
import com.if5.todoapp.models.dtos.AppTaskSetDto;
import com.if5.todoapp.models.dtos.NumberTaskDto;
import com.if5.todoapp.models.entities.AppTask;
import com.if5.todoapp.models.enumeration.TaskStatus;
import com.if5.todoapp.services.interfaces.TaskServiceInterface;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/toDoApp/V1")
@Api("/toDoApp/V1/task")
public class TaskController {

	 @Autowired private TaskServiceInterface taskServiceInterface;
	
	TaskController(TaskServiceInterface taskServiceInterface) {
		this.taskServiceInterface = taskServiceInterface;
	}
	
	
	@ApiOperation(value = "create new task in database", notes = "elle permet la creation d'une nouvelle tâche  dans la base de donnée", response = AppTask.class)
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "La tâche est creer avec succès"),
			@ApiResponse(code = 404, message = "La tâche fournie n'est pas  valide")
	})
	@PostMapping("/task")
	public ResponseEntity<AppTask> saveTask(@RequestBody @Valid AppTaskSetDto appTaskSetDto, HttpServletRequest request) throws InvalidEntityException, EntityNotFoundException {
		
		
		return new ResponseEntity<>(taskServiceInterface.saveTask(appTaskSetDto,request), HttpStatus.CREATED);
		
	}
	
	
	@ApiOperation(value = "get all task", notes = "récupere toutes les tâches dans la base de donnée", response = AppTask.class)
	@ApiResponses(value= {
			@ApiResponse(code = 201, message = "les tâches  ont été récupéré avec succès")
	})
	  @GetMapping("/tasks")
	  public ResponseEntity<List<AppTask>> getAllTask(){
	  return ResponseEntity.ok(taskServiceInterface.getAllTask()); }
	 
	
	@ApiOperation(value = "delete task", notes = "permet la suppression d'une tâche  dans la base de donnée")
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "La tâche est supprimer avec succès"),
			@ApiResponse(code = 404, message = "L'identifiant fournie ne correspond à aucune tâche dans la base de donnée ")
	})
	  @DeleteMapping("/task/delete/{id}") 
	  public void deleteTask(@PathVariable Long id) {
		          taskServiceInterface.deleteTask(id);
		  }
	 
	@ApiOperation(value = "update task", notes = "permet de modifier une tâche qui existe déja dans la base de donnée",response = AppTask.class)
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "La tâche est modifier avec succès"),
			@ApiResponse(code = 404, message = "L'identifiant fournie ne correspond à aucune tâche dans la base de donnée ")
	})
	  @PutMapping("/task/update/{id}") 
	  public ResponseEntity<AppTask> updateTask(@PathVariable Long id, @RequestBody AppTaskDto appTaskDto) throws InvalidEntityException {
		  return new ResponseEntity<>(taskServiceInterface.updateTask(id, appTaskDto),HttpStatus.OK);
	  }
	
	
	@ApiOperation(value = "change task status", notes = "permet de modifier le statut d'une tâche  dans la base de donnée", response = AppTask.class)
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "La tâche est supprimer avec succès"),
			@ApiResponse(code = 404, message = "L'identifiant fournie ne correspond à aucune tâche dans la base de donnée ")
	})
	  @PutMapping("/change/statut/{id}")
	  public void changeTaskStatus(@PathVariable Long id, @RequestParam TaskStatus statut ) {
		  System.out.print(id+" "+ statut);
		  taskServiceInterface.changeTaskStatus(id, statut );
		  System.out.print(id+" "+ statut);
		  
	  }
	  
	
	@ApiOperation(value = "task according to status", notes = "Permet de récupérer une liste de tâche  selon le statut (REALISEE, EN_ATTENTE,EN_COURS,ANNULE)de la tâche", responseContainer = "List<AppTask>")
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "Les tâches sont récupérer avec succès"),
	})
	  @GetMapping("/tasks/status")
	  public ResponseEntity<List<AppTask>> taskAccordingToStatus(@RequestParam String status)  {
		  
		  TaskStatus statut = TaskStatus.valueOf(status);
		  
		     return ResponseEntity.ok( taskServiceInterface.taskAccordingToStatus(statut));
	}
	  
	

	@ApiOperation(value = "Calcul number of tasks performed in a period", notes = "permet de modifier le statut d'une tâche  dans la base de donnée", response =Integer.class)
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "Le nombre de tâche à été calculer avec succès"),
			@ApiResponse(code = 404, message = "Les informations fournies ne sont pas correcte")
	})
	  @PostMapping("/number/task")
	  public ResponseEntity<Integer> numberOfTasksPerformedInAPeriod(@RequestBody NumberTaskDto numberTaskDto){

		 Integer numberOfTask = taskServiceInterface.numberOfTasksPerformedInAPeriod(numberTaskDto); 
		  return ResponseEntity.ok(numberOfTask);
	  }
	
	  

	@ApiOperation(value = "show details of a task", notes = "permet de chercher une tâche par son identifiant dans la base de donnée", response =Integer.class)
	@ApiResponses(value= {
			
			@ApiResponse(code = 201, message = "La tâche à été trouver avec succès"),
			@ApiResponse(code = 404, message = "aucune tâche avec l'identifiant fournie n'a été trouvé en base de donnée")
	})
	  @GetMapping("/task-details/{id}")
	  public ResponseEntity<AppTask> showDetailsOfAtask(@PathVariable long id) throws EntityNotFoundException{  
		  return ResponseEntity.ok(taskServiceInterface.showDetailsOfAtask(id));
	  }
	  
}
