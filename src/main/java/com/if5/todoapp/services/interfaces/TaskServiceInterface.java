package com.if5.todoapp.services.interfaces;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.if5.todoapp.exceptions.EntityNotFoundException;
import com.if5.todoapp.exceptions.InvalidEntityException;
import com.if5.todoapp.models.dtos.AppTaskDto;
import com.if5.todoapp.models.dtos.AppTaskSetDto;
import com.if5.todoapp.models.dtos.NumberTaskDto;
import com.if5.todoapp.models.entities.AppTask;
import com.if5.todoapp.models.enumeration.TaskStatus;

public interface TaskServiceInterface {

	public List<AppTask> getAllTask();
	public AppTask saveTask(AppTaskSetDto appTaskDto,  HttpServletRequest request) throws InvalidEntityException, EntityNotFoundException;
	public AppTask addTaskStatut(String taskname, TaskStatus taskstatus);
	public AppTask findByTaskTitle(String taskTitle);
	public void deleteTask(Long id);
	public AppTask updateTask(Long id,AppTaskDto appTaskDto) throws InvalidEntityException;
	public AppTask showDetailsOfAtask (Long id) throws EntityNotFoundException;
	public void taskManager();
	public int numberOfTasksPerformedInAPeriod (NumberTaskDto numberTaskDto);
	public void calculateTheNumberOfWorkingHours(Date debut, Date fin);
	public void changeTaskStatus( Long id, TaskStatus status);
	public void addParentTask(AppTask appTask, AppTask appTask1);
	public List<AppTask> taskAccordingToStatus( TaskStatus status);
	
}
