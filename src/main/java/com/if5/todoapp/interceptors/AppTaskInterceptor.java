package com.if5.todoapp.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class AppTaskInterceptor implements HandlerInterceptor {
 	
	 
	   public boolean preHandle(
	      HttpServletRequest request,
	      HttpServletResponse response, Object handler) throws Exception {
		   
		   
	      return true;
	   }
	   
	   public void postHandle(
	      HttpServletRequest request, HttpServletResponse response, Object handler, 
	      ModelAndView modelAndView) throws Exception {}
	   
	   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
	      Object handler, Exception exception) throws Exception {}
}
