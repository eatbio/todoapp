package com.if5.todoapp.config;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	
	public Docket api() {
		
     return new Docket(DocumentationType.SWAGGER_2)
    		 .apiInfo(
    				 new ApiInfoBuilder()
    				 .description("ToDoList API Documantation ")
    				 .title("ToDoList REST API")
    				 .build()
    				 )
    		 .groupName("REST API V1")
    		 .select()
    		 .apis(RequestHandlerSelectors.basePackage("com.if5.todoapp"))
    		 .paths(PathSelectors.ant("/toDoApp/V1/**"))
    		 .build();
	}

}
