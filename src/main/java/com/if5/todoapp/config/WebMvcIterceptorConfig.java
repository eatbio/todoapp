package com.if5.todoapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.if5.todoapp.interceptors.AppTaskInterceptor;

@Configuration
public class WebMvcIterceptorConfig extends WebMvcConfigurerAdapter{

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(new AppTaskInterceptor());
	}
}
