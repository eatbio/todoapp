package com.if5.todoapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.if5.todoapp.models.entities.AppTask;
import com.if5.todoapp.models.enumeration.TaskStatus;

@Repository
@EnableJpaRepositories
public interface AppTaskRepository extends JpaRepository<AppTask, Long> {

	
	@Query("SELECT at FROM AppTask at WHERE at.taskTitle =:taskTitle")
	public AppTask findByTaskTitle(@Param("taskTitle") String tasktitle);
	
	
	public List<AppTask> findAllByTaskStatus(TaskStatus taskStatus);
	
	@Query(value="SELECT a FROM AppTask a WHERE a.startDate>=:startDate AND a.endDate<=:endDate AND a.taskStatus=com.if5.todoapp.models.enumeration.TaskStatus.REALISEE")
	public List<AppTask> findAllByTaskStatut(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	
}
